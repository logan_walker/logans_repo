﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorial2Exercise2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static Dictionary<string, string> fruitColors()
        {
            var dict = new Dictionary<string, string>();
            dict.Add("banana", "yellow");
            dict.Add("tomato", "red");
            dict.Add("apple", "green");

            return dict;
        }

        private void btnCheck_Click(object sender, EventArgs e)
        {
            lblAnswer.Text = checkFruit();
        }

        public string checkFruit()
        {
            var dict = fruitColors();
            var result = "";

            if (dict.ContainsKey(textBox1.Text))
            {
                result = "Yes! the fruit is in the list";
            }
            else
            {
                result = "Nope! the fruit is not in the list";
            }

            return result;
        }
    }
}
