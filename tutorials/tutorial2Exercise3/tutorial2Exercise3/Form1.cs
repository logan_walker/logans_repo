﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace tutorial2Exercise3
{
    public partial class Form1 : Form
    {

        List<string> item = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        public List<string> items()
        {
            item.Add(txtWordInput.Text);
            return item;
        }

        private void btnAddToList_Click(object sender, EventArgs e)
        {
            lstItems.BeginUpdate();

            if (lstItems.Items.Count != 0)
            {
                lstItems.Items.Clear();
            }

            foreach (var item in items())
            {
                lstItems.Items.Add(item);
            }

            lstItems.EndUpdate();

            txtWordInput.Clear();
            txtWordInput.Focus();
        }
    }
}
